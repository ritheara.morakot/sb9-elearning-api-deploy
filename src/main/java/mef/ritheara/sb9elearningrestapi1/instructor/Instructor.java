package mef.ritheara.sb9elearningrestapi1.instructor;

import jakarta.persistence.*;
import lombok.*;
import mef.ritheara.sb9elearningrestapi1.course.Course;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "instructors" )
public class Instructor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String familyName;
    private String givenName;

    private Boolean isDeleted;

    @Column(length = 200, unique = true, nullable = false)
    private String nationalIdCard;

    @Column(columnDefinition = "TEXT")
    private String biography;


    @OneToMany(mappedBy = "instructor")
    private List<Course> courses;

}

