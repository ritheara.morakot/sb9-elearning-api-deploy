package mef.ritheara.sb9elearningrestapi1.instructor;

import lombok.Builder;

@Builder
public record InstructorDto(
        Integer id,
        String familyName,
        String givenName,
        String biography

        )

{
}
