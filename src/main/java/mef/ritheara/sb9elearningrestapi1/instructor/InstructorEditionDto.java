package mef.ritheara.sb9elearningrestapi1.instructor;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;

@Builder
public record InstructorEditionDto(

        @NotBlank
        String familyName,
        @NotBlank
        String givenName,
        String biography
) {
}
