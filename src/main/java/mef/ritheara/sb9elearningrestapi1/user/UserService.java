package mef.ritheara.sb9elearningrestapi1.user;

public interface UserService {

    void createUser(UserCreationDto userCreationDto);

}
