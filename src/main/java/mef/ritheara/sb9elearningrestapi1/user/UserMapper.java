package mef.ritheara.sb9elearningrestapi1.user;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User mapFromuserCreationDto (UserCreationDto userCreationDto);

}

