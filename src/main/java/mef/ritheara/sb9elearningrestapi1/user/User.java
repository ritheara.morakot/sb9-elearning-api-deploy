package mef.ritheara.sb9elearningrestapi1.user;

import jakarta.persistence.*;
import lombok.*;
import mef.ritheara.sb9elearningrestapi1.role.Role;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column( unique = true, nullable = false)
    private String username;
    @Column( unique = true, nullable = false)
    private String email;

    private String password;
    private String verifyCode;
    private Boolean isVerified;
    private Boolean isDeleted;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
                )
    private Set<Role> roles;
}
