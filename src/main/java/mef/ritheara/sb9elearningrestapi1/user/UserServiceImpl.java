package mef.ritheara.sb9elearningrestapi1.user;


import lombok.RequiredArgsConstructor;
import mef.ritheara.sb9elearningrestapi1.role.Role;
import mef.ritheara.sb9elearningrestapi1.role.RoleRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    @Transactional
    @Override
    public void createUser(UserCreationDto userCreationDto) {

        User user = userMapper.mapFromuserCreationDto(userCreationDto);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setIsDeleted(false);
        user.setIsVerified(false);

        if (userRepository.existsByEmail(user.getEmail())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Email already existed");
        }

        if (userRepository.existsByUsername(user.getUsername())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Username is already Taken");
        }
        Set<Role> roles = new HashSet<>();
        userCreationDto.roleName().forEach(name -> {
            Role role = roleRepository.findByName(name).orElseThrow(
                    ()->  new ResponseStatusException(HttpStatus.NOT_FOUND, "role invalid!")
            );
            roles.add(role);
        });

        user.setRoles(roles);
        userRepository.save(user);
    }

}
