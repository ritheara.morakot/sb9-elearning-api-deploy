package mef.ritheara.sb9elearningrestapi1.role;

import java.util.List;

public record RoleDto(
        String name,
        List<String> authorities

) {
}
