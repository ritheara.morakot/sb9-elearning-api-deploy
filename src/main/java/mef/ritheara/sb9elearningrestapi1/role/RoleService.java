package mef.ritheara.sb9elearningrestapi1.role;

import java.util.List;


public interface RoleService {
    List<RoleDto> findAll();

    RoleDto findByName(String name);
}
