package mef.ritheara.sb9elearningrestapi1.auth;

import jakarta.persistence.*;
import lombok.*;
import mef.ritheara.sb9elearningrestapi1.role.Role;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Entity
@Table(name = "authorities")
public class Authority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @ManyToMany(mappedBy = "authorities")
    private List<Role> roles;

    //@OneToMany(mappedBy = "authority")
    //private List<RoleAuthority> roleAuthorities;
}
