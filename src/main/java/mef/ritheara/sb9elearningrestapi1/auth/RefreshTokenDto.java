package mef.ritheara.sb9elearningrestapi1.auth;

import jakarta.validation.constraints.NotBlank;

public record RefreshTokenDto(
        @NotBlank
        String refreshToken
) {
}
