package mef.ritheara.sb9elearningrestapi1.auth;


import jakarta.mail.MessagingException;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import mef.ritheara.sb9elearningrestapi1.user.UserRegisterDto;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;
    private final JwtEncoder jwtEncoder;
   /* @PostMapping("/login")
    Map<String, Object> login(@Valid @RequestBody LoginDto loginDto){
        Instant now = Instant.now();
        JwtClaimsSet jwtClaimsSet = JwtClaimsSet
                .builder()
                .id(loginDto.email())
                .audience(List.of("Mobile", "Web"))
                .issuedAt(now)
                .expiresAt(now.plus(30, ChronoUnit.MINUTES))
                .issuer(loginDto.email())
                .build();
        String jwtToken = jwtEncoder.encode(JwtEncoderParameters.from(jwtClaimsSet)).getTokenValue();
        return Map.of("token", jwtToken);
    }*/
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/register")
    Map<String, Object> register(@Valid @RequestBody UserRegisterDto userRegisterDto) throws MessagingException {
        return authService.userRegistration(userRegisterDto);
    }
    @PostMapping("/verify")
    Map<String, Object> verify(@Valid @RequestBody VerifyDto verifyDto){
        return authService.verify(verifyDto);
    }
    @PostMapping("/login")
    AuthDTo login(@Valid @RequestBody LoginDto loginDto){
        return authService.login(loginDto);
    }
    @PostMapping("/refresh")
    AuthDTo refresh(@Valid @RequestBody RefreshTokenDto refreshTokenDto){
        return authService.refresh(refreshTokenDto);
    }
}
