package mef.ritheara.sb9elearningrestapi1.auth;

import jakarta.validation.constraints.NotBlank;

public record LoginDto(
        @NotBlank
        String email,
        @NotBlank
        String password
) {
}
