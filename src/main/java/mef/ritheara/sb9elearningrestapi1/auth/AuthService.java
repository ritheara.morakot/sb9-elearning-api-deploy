package mef.ritheara.sb9elearningrestapi1.auth;


import jakarta.mail.MessagingException;
import mef.ritheara.sb9elearningrestapi1.user.UserRegisterDto;

import java.util.Map;


public interface AuthService {
    AuthDTo refresh(RefreshTokenDto refreshTokenDto);
    AuthDTo login(LoginDto loginDto);

    Map<String, Object> userRegistration(UserRegisterDto userRegisterDto) throws MessagingException;

    Map<String, Object> verify(VerifyDto verifyDto);
}
