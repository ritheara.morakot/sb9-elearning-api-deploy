package mef.ritheara.sb9elearningrestapi1.auth;

import lombok.Builder;

@Builder
public record AuthDTo(
        String tokenType,
        String accessToken,
        String refreshToken
) {
}
