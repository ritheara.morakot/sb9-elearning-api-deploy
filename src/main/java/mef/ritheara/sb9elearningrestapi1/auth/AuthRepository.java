package mef.ritheara.sb9elearningrestapi1.auth;

import mef.ritheara.sb9elearningrestapi1.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface AuthRepository extends JpaRepository<User, Long> {
    @Modifying
    @Query("""
            UPDATE User AS u
            SET u.verifyCode = :verifiedCode
            WHERE u.email = :email
            """)
    void updateVerifiedCode(String email, String verifiedCode);

    Optional<User> findByEmailAndVerifyCode(String email, String verifiedCode);
}
