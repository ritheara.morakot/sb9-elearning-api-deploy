package mef.ritheara.sb9elearningrestapi1.auth;

public record AuthorityDto(

        String name

) {
}
