package mef.ritheara.sb9elearningrestapi1.auth;


import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mef.ritheara.sb9elearningrestapi1.common.RandomUtil;
import mef.ritheara.sb9elearningrestapi1.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.neo4j.Neo4jProperties;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationProvider;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthServiceImpl implements AuthService {


    private final UserService userService;
    private final UserRegistrationMapper userRegistrationMapper;
    private final JavaMailSender javaMailSender;
    private final AuthRepository authRepository;
    private final JwtAuthenticationProvider jwtAuthenticationProvider;
    private final JwtEncoder jwtEncoder;
    private JwtEncoder jwtRefreshTokenEncoder;
    @Autowired
    @Qualifier("jwtRefreshEncoder")
    public void setJwtRefreshTokenEncoder(JwtEncoder jwtRefreshTokenEncoder){
        this.jwtRefreshTokenEncoder = jwtRefreshTokenEncoder;
    }
    @Value("spring.mail.username")
    private String adminMail;
    @Transactional
    @Override
    public Map<String, Object> userRegistration(UserRegisterDto userRegisterDto) throws MessagingException {


        if (!userRegisterDto.password().equals(userRegisterDto.confirmPassword())){
            throw new  ResponseStatusException(HttpStatus.BAD_REQUEST, "Password doesn't match");
        }


        UserCreationDto users = userRegistrationMapper.fromUserRegistrationDto(userRegisterDto);
        userService.createUser(users);
        String sixDigit = RandomUtil.random6Digit();
        authRepository.updateVerifiedCode(userRegisterDto.email(), sixDigit);
        // Send mail with verified code
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message);
        mimeMessageHelper.setSubject("Account Verification");
        mimeMessageHelper.setText(sixDigit);
        mimeMessageHelper.setTo(userRegisterDto.email());
        mimeMessageHelper.setFrom(adminMail);
        javaMailSender.send(message);
        return  Map.of("message", "Please Verify Via Email address",
                            "email", users.email()
                        );

    }


    @Override
    public Map<String, Object> verify(VerifyDto verifyDto) {
        User user = authRepository.findByEmailAndVerifyCode(
                verifyDto.email(), verifyDto.verifiedCode()
        ).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User has not been found"));
        user.setIsVerified(true);
        user.setVerifyCode(null);
        authRepository.save(user);
        return Map.of(
                "message", "your account has been verified",
                "email", verifyDto.email()
        );
    }
    private  final DaoAuthenticationProvider daoAuthenticationProvider;
    private String createAccessToken(Authentication authentication){
        Instant now = Instant.now();
        String scope = authentication.getAuthorities()
                .stream().map(authority->authority.getAuthority())
                .collect(Collectors.joining(" "));
        JwtClaimsSet jwtClaimsSet = JwtClaimsSet
                .builder()
                .id(authentication.getName())
                .audience(List.of("Mobile", "Web"))
                .issuedAt(now)
                .expiresAt(now.plus(30, ChronoUnit.MINUTES))
                .issuer(authentication.getName())
                .subject("Access Token")
                .claim("scope", scope)
                .build();
        String accessToken = jwtEncoder.encode(JwtEncoderParameters.from(jwtClaimsSet)).getTokenValue();
       /*return AuthDTo
                .builder()
                .tokenType("Bearer")
                .accessToken(accessToken)
                .build(); */
        return accessToken;
    }
    private String createRefreshToken(Authentication authentication){
        Instant now = Instant.now();
        JwtClaimsSet jwtClaimsSet = JwtClaimsSet.builder()
                .id(authentication.getName())
                .audience(List.of("Mobile", "Web"))
                .issuedAt(now)
                .expiresAt(now.plus(30, ChronoUnit.DAYS))
                .issuer(authentication.getName())
                .subject("Refresh Token")
                .build();
        //return jwtEncoder.encode(JwtEncoderParameters.from(jwtClaimsSet)).getTokenValue();
        return jwtRefreshTokenEncoder.encode(JwtEncoderParameters.from(jwtClaimsSet)).getTokenValue();
    }

    @Override
    public AuthDTo login(LoginDto loginDto) {
        Authentication auth = new UsernamePasswordAuthenticationToken(
                loginDto.email(),
                loginDto.password()
        );
        auth =  daoAuthenticationProvider.authenticate(auth);
        log.info("Auth: {}", auth.getName());
        log.info("Auth: {}", auth.getAuthorities());

        //return this.createAccessToken(auth);
        return AuthDTo
                .builder()
                .tokenType("Bearer")
                .accessToken(this.createAccessToken(auth))
                .refreshToken(this.createRefreshToken(auth))
                .build();

    }

    @Override
    public AuthDTo refresh(RefreshTokenDto refreshTokenDto) {
        Authentication auth = new BearerTokenAuthenticationToken(refreshTokenDto.refreshToken());
        auth = jwtAuthenticationProvider.authenticate(auth);
        return AuthDTo
                .builder()
                .tokenType("Bearer")
                .accessToken(this.createAccessToken(auth))
                .refreshToken(this.createRefreshToken(auth))
                .build();
    }
}
