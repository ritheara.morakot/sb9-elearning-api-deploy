package mef.ritheara.sb9elearningrestapi1.security;
import lombok.RequiredArgsConstructor;
import mef.ritheara.sb9elearningrestapi1.user.User;
import mef.ritheara.sb9elearningrestapi1.user.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImp implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository
                .findByEmailAndIsDeletedAndIsVerified(username, false, true)
                .orElseThrow(() -> new UsernameNotFoundException("Invalid user!"));
        CustomUserDetail customUserDetail = new CustomUserDetail();
        customUserDetail.setUser(user);

        return customUserDetail;

    }
}
