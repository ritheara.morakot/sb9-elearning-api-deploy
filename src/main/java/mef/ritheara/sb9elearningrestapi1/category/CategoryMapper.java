package mef.ritheara.sb9elearningrestapi1.category;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CategoryMapper {

    List<Category> toCategoryListDto (List<Category> categories);
    Category fromCategoryDto (CategoryCreationDto category);

    Category fromCategoryEditionDto ( @MappingTarget Category category, CategoryEditionDto categoryEditionDto);

    Category toCategoryDto ( Category category);





}
