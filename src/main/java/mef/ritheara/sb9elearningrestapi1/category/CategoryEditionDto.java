package mef.ritheara.sb9elearningrestapi1.category;

import lombok.Builder;

@Builder
public record CategoryEditionDto(
        CategoryDto categoryDto

) {
}
