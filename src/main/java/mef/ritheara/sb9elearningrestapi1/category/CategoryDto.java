package mef.ritheara.sb9elearningrestapi1.category;

import lombok.Builder;

@Builder
public record CategoryDto(

        Integer id,
        String name,
        Boolean isDeleted

) {
}
