package mef.ritheara.sb9elearningrestapi1.fileupload;

import lombok.Builder;

@Builder
public record FileDto(
        String name,
        String extension,
        Long size,
        String uri
) {
}
