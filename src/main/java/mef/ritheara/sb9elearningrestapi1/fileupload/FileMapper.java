package mef.ritheara.sb9elearningrestapi1.fileupload;

import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FileMapper {

    List<FileDto> toFileListDto(List<FileDto> files);
}
