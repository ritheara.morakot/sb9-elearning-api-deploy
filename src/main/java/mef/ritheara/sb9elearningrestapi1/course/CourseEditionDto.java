package mef.ritheara.sb9elearningrestapi1.course;

import lombok.Builder;
import mef.ritheara.sb9elearningrestapi1.category.CategoryDto;
import mef.ritheara.sb9elearningrestapi1.instructor.InstructorDto;

@Builder
public record CourseEditionDto(

        String title,

        String thumbnail,

        String description,

        Boolean isFree,

        CategoryDto category,

        InstructorDto instructor,

        Boolean isDeleted

) {


}
