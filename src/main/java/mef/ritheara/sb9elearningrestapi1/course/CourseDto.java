package mef.ritheara.sb9elearningrestapi1.course;


import lombok.Builder;
import mef.ritheara.sb9elearningrestapi1.category.CategoryDto;
import mef.ritheara.sb9elearningrestapi1.instructor.InstructorDto;

@Builder
public record CourseDto(
        Long id,
        String title,
        String description,
        String thumbnail,
        Boolean isFree,
        CategoryDto category,
        InstructorDto instructor,
        Boolean isDeleted
) {
}
