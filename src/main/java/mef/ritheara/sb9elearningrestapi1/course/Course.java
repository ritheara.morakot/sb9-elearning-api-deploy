package mef.ritheara.sb9elearningrestapi1.course;

import jakarta.persistence.*;
import lombok.*;
import mef.ritheara.sb9elearningrestapi1.category.Category;
import mef.ritheara.sb9elearningrestapi1.instructor.Instructor;

@Setter
@Getter
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Entity
@Table( name = "courses")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    private String thumbnail;

    @Column(columnDefinition = "TEXT")
    private String description;

    private Boolean isFree;
    private Boolean isDeleted;

    @ManyToOne
    @JoinColumn(name = "ins_id", referencedColumnName = "id")
    private Instructor instructor;

    @ManyToOne
    @JoinColumn(name = "cat_id", referencedColumnName = "id")
    private Category category;
}
