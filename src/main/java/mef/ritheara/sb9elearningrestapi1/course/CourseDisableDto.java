package mef.ritheara.sb9elearningrestapi1.course;

public record CourseDisableDto(
        Boolean isDeleted
) {

}
